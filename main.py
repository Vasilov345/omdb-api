import os.path

# tornado libs
import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.options
from tornado import httpclient
from tornado.options import define, options

# file with application configuration
from settings import *

# third part modules
import ast
import uuid
from urllib import request
from datetime import datetime
import logging

# Modules for mongodb
import pymongo
from mongolog.handlers import MongoHandler


define("port", default=8000, help="run on the given port", type=int)


class Application(tornado.web.Application):
    def __init__(self):

        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            ui_modules={"Film": FilmModule},
            debug=False,
            xsrf_cookies=True,
        )
        handlers = [
            (r"/", MainHandler),
            (r"/get_img", ImageHandler),
            (r"/get_history", HistoryHandler),
            (r'/static/images/(.*)', tornado.web.StaticFileHandler, {'path': settings['static_path']})
        ]
        # DB setup
        conn = pymongo.MongoClient(MONGO_DB["HOST"], MONGO_DB["PORT"])
        self.db = conn[MONGO_DB["BASE"]]
        self.path = "static/images"
        tornado.web.Application.__init__(self, handlers, **settings)


class MainHandler(tornado.web.RequestHandler):

    def get(self):
        self.render(
            "get_image_form.html",
        )

    def data_received(self, chunk):
        pass


class ImageHandler(tornado.web.RequestHandler):

    def get(self):

        title = self.get_argument("title")

        film_info = self.get_image(title)

        self.render("show_image.html", film_info=film_info)

    def get_image(self, title):
        """
        Use title to found information about film from OMDB_API
        :param title: String type. Films name
        :return: Dictionary  with information about film or None
        """
        http_client = httpclient.HTTPClient()

        try:
            url = OMDB_API_URL + 't=' + title.replace(" ", "+")
            response = http_client.fetch(url)
            # film info arrived in bytes, not even in string. So we have to erase redundant bytes and convert to dict
            film_info = ast.literal_eval(str(response.body)[2:-1])
            status = film_info.get("Error", None)
            if status:
                raise Exception("{}".format(status))
            film_object = self.save_object(film_info)
            return film_object
        except httpclient.HTTPError as e:
            # HTTPError is raised for non-200 responses; the response
            logging.error("{}".format(e))
        except Exception as e:
            # Other errors are possible, such as IOError.
            logging.error("{}".format(e))
        finally:
            http_client.close()

    def save_object(self, film_info):
        """
        Check if film is already in database. And if it was not changed.
        If not - create new object with film info and save into database.

        :param film_info: Dictionary with film details from API
        :return: Dictionary that will be saved in database
        example : {
        "title": "Superman",
        "img": "path_to_file_on_server",
        "date": "date when film was saved in db", hash}
        "hash": "3617284125789058749"  Hash of all information about film. For changes tracking
         """
        db = self.application.db
        film_object = db.widgets.find_one({"title": film_info["Title"]})
        if film_object and film_object['hash'] != hash(str(film_info)):
            db.widgets.remove(film_object)
            film_object = {}

        if not film_object:
            image_src = self.download_image(film_info['Poster'])
            if image_src:
                film_object = {
                    'title': film_info['Title'],
                    'hash': hash(str(film_info)),
                    'img': image_src,
                    'date': datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M"),
                }
            db.widgets.insert_one(film_object)
        return film_object

    def download_image(self, url):
        """
        Download and save poster from url.
        :param url: String type. Valid url for film poster
        :return: path to poster or None
        """
        local_path = self.application.path + '/{code}.jpg'.format(code=uuid.uuid4())
        try:
            request.urlretrieve(url, local_path)
        except ValueError as e:
            logging.error(" Bad url. {}".format(e))
        return local_path

    def data_received(self, chunk):
        pass


class HistoryHandler(tornado.web.RequestHandler):

    def data_received(self, chunk):
        pass

    def get(self):
        db = self.application.db
        films = db.widgets.find().sort("date", -1)
        print(films)
        self.render("history.html", films=films)


class FilmModule(tornado.web.UIModule):
    def render(self, film):
        return self.render_string('modules/film.html', film=film)


def main():

    log = logging.getLogger('logger')
    log.setLevel(logging.DEBUG)

    log.addHandler(MongoHandler.to('mongolog', 'log'))

    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
